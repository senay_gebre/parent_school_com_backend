const mongoose = require('mongoose')
const dbConfig = require('./dbconfig')


const connectDB = async() => {
    try {
        const conn = await mongoose.connect(dbConfig.database, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        var cntest = conn.connection.host
        // console.log('MongoDB Connected: '+ cntest)
        console.log('MongoDB Connected: '+ cntest)
    }
    catch (err) {
        console.log(err)
        process.exit(1)
    }
}

module.exports = connectDB
 